'use strict';

const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

var mainWindow = null;
var config = { height: 600, width: 800, url: 'http://fxp.co.il'};

app.on('window-all-closed', function() {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform != 'darwin') {
        app.quit();
    }
});

let next = 'name';
for(let i=2; i<process.argv.length; i++) {
    if(next == 'name') {
        if(process.argv[i] == '--url') {
            next = 'url';
        } else if(process.argv[i] == '--height') {
            next = 'height';
        } else if(process.argv[i] == '--width') {
            next = 'width';
        }
    } else if(next == 'url') {
        config.url = process.argv[i];
        next = 'name';
    } else if(next == 'height') {
        config.height= Number(process.argv[i]);
        next = 'name';
    } else if(next == 'width') {
        config.width = Number(process.argv[i]);
        next = 'name';
    }
}

app.on('ready', function() {
    console.log(config.url);
    mainWindow = new BrowserWindow({width: config.width, height: config.height});

    mainWindow.loadURL(config.url);

    mainWindow.on('closed', function() {
        mainWindow = null;
    });
});
