# Self Window
This is a small script that open websites in a window without the browser ui.

## How to Run
Install nodejs, to do this follow the instructions from the [official download
page](https://nodejs.org/en/download/) or install it from your distro's package
manager.

Than install electron with npm (nodejs package manager):

```
npm install electron-prebuilt -g
```

After you have installed Node.JS and Electron run the program from the terminal:

```
electron . --url <Website url> --height <default 600> --width <default 800>
```